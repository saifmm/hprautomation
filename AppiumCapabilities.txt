Capabilities

HCP DEV ioS
{
  "appiumVersion": "1.13.0",
  "deviceName": "iPhone XR Simulator",
  "deviceOrientation": "portrait",
  "platformVersion": "12.2",
  "platformName": "iOS",
  "app": "sauce-storage:hcp_dev.zip",
  "bundleId": "com.jnj.mdd.emea.careadvantage.hcp-dev"
}


Patient DEV IOS
{
 "appiumVersion": "1.13.0",
  "deviceName": "iPhone XR Simulator",
  "deviceOrientation": "portrait",
  "platformVersion": "12.2",
  "platformName": "iOS",
  "app": "sauce-storage:patient_dev.zip",
  "bundleId": "com.jnj.mdd.emea.careadvantage-dev"
}

Android HCP QA
{
  "appiumVersion": "1.9.1",
  "deviceName": "Samsung Galaxy S9 WQHD GoogleAPI Emulator",
  "deviceOrientation": "portrait",
  "platformVersion": "9.0",
  "platformName": "Android",
  "app": "sauce-storage:app-qa-release.apk",
  "appActivity": "com.care4today.MainActivity"
}

Patient app Dev
{
  "appiumVersion": "1.9.1",
  "deviceName": "Samsung Galaxy S9 WQHD GoogleAPI Emulator",
  "deviceOrientation": "portrait",
  "platformVersion": "9.0",
  "platformName": "Android",
  "app": "sauce-storage:app-patientDev-debug.apk",
  "appActivity": "com.jnj.careadvmodule.profilesetup.ProfileSetupActivity"
}